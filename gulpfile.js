//Connect the gulp modules
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const rupture = require('rupture');
const browserSync = require('browser-sync').create();

const sourcemaps = require('gulp-sourcemaps');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');

// Connection order of the sass/scss-files
const pugFiles = [
    './src/**/*.pug'
];

// Connection order of the sass/scss-files
const cssFiles = [
    './src/styles/**/*.styl'
];

//Connection order of the js files
const jsFiles = [
    './src/scripts/**/*.js'
];

//Task for PUG files
function html() {
    return gulp.src(pugFiles)
        .pipe(pug({
            pretty: true
        }))
        //Output folder for styles
        .pipe(gulp.dest('./build'))
        .pipe(browserSync.stream());
}

//Task for CSS styles
function styles() {
    return gulp.src(cssFiles)
        .pipe(sourcemaps.init())
        .pipe(stylus({
            use: rupture()
        }))
        //Merge files into one
        .pipe(concat('main.css'))
        //Add prefixes
        .pipe(autoprefixer())
        //CSS minification
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(sourcemaps.write('./'))
        //Output folder for styles
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());
}

//Task for JS scripts
function scripts() {
    return gulp.src(jsFiles)
        //Merge files into one
        .pipe(concat('main.min.js'))
        //JS minification
        .pipe(uglify({
            toplevel: true
        }))
        //Output folder for scripts
        .pipe(gulp.dest('./build/scripts'))
        .pipe(browserSync.stream());
}

function copy_img() {
    return gulp.src('./src/img/**/*')
        .pipe(gulp.dest('./build/img'));
}

function copy_fonts() {
    return gulp.src('./src/font/**/*')
        .pipe(gulp.dest('./build/font'));
}

//Delete all files from specified folder
function clean() {
    return del(['build/*'])
}

//Watch files
function watch() {
    browserSync.init({
        server: {
            baseDir: "./build"
        },
    });

    //Watch PUG files
    gulp.watch('./src/**/*.pug', html);
    //Watch CSS files
    gulp.watch('./src/styles/**/*.css', styles);
    gulp.watch('./src/styles/**/*.styl', styles);
    //Watch JS files
    gulp.watch('./src/scripts/**/*.js', scripts);
    //Start synchronization after HTML changing
    gulp.watch("./src/**/*.pug").on('change', browserSync.reload);
}


//Task calling 'pug' function
gulp.task('html', html);
//Task calling 'styles' function
gulp.task('styles', styles);
//Task calling 'scripts' function
gulp.task('scripts', scripts);
//Task calling 'copy_img' function
gulp.task('copy_img', copy_img);
//Task calling 'copy_fonts' function
gulp.task('copy_fonts', copy_fonts);
//Task for cleaning the 'build' folder
gulp.task('del', clean);
//Task for changes tracking
gulp.task('watch', watch);
//Task for cleaning the 'build' folder and running 'styles' and 'scripts' functions
gulp.task('build', gulp.series(clean, gulp.parallel(html, styles, scripts, copy_img, copy_fonts)));
//Task launches build and watch task sequentially
gulp.task('dev', gulp.series('build', 'watch'));
//Default task
gulp.task('default', gulp.series('build', 'watch'));